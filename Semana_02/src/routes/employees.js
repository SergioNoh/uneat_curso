const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database.js');

router.get('/', (req, res)=>{
    mysqlConnection.query('SELECT * FROM employees', (err,rows, fields)=>{
        if(!err){
            res.json(rows);
        } else {
            console.log(err)
        }
    })
})

router.get('/:id', (req, res) => {
  // const id = req.params;//{id}
  const { id } = req.params;//"valor id" 3224124
  const query = 'SELECT * FROM employees WHERE id = ?'
    mysqlConnection.query(query, [id], (err, rows, fields) => {
      if (!err) {
        res.json(rows[0]);
      } else {
        console.log(err);
      }
    });
  });

module.exports = router;