import React from 'react';
import ReactDOM from 'react-dom';


// const element = document.createElement('h1')
// element.innerText = 'Hola mundo'

// const container = document.getElementById('root')
// container.appendChild(element)


// const element = React.createElement('h1',{},'Hello world');

const name = "Billy"
const sum = () => 3+3;

const element = React.createElement('h1',{},`Hola ${name}, bienvenido al curso`);
// const jsx = <h1>Hello world from jsx</h1>
// const jsx = <h1>Hola {name} bienvenido</h1>
// const jsx = <h1>Hola bienvenido {sum}</h1> //mal
const jsx = <h1>Hola bienvenido {sum()}</h1> //bien

const container = document.getElementById('root')

// ReactDOM.render(__que__, __donde__)
ReactDOM.render(jsx, container)