const { Router } = require('express');
const { identity } = require('underscore');
const router = new Router();
const _ = require('underscore');//dependencia recorre los json

const movies = require('../movies.json');

// console.log(movies)

router.get('/', (req, res) => {
    res.json(movies);
});

router.post('/', (req, res) => {

    // console.log(req.body)

    const id = movies.length + 1;
    
    const { title, director, year, rating } = req.body;
    
    const newMovie = { ...req.body, id };
    if (id && title && director && year && rating) {
        movies.push(newMovie);
        res.json(movies);
    } else {
        res.status(500).json({ error: 'There was an error :c' });
    }
});

router.put('/:id', (req, res) => {

    console.log(req.params)

    const { id } = req.params;
    const { title, director, year, rating } = req.body;
    if (id && title && director && year && rating) {
        _.each(movies, (movie, i) => {
            if (movie.id === id) {
                movie.title = title;
                movie.director = director;
                movie.year = year;
                movie.rating = rating;
                res.json({status: "movie updated"});
            }
        });
        res.json({status: "the id not exists"});
    } else {
        res.status(500).json({ error: 'There was an error.' });
    }
});

router.delete('/:id', (req, res) => {
    // console.log(req.params)
    // res.send('deleted')
    const { id } = req.params; //1
    // const id = req.params; //{id:1}
    if (id) {
        _.each(movies, (movie, i) => {
            if (movie.id == id) {
                movies.splice(i, 1);
                res.json()
            }
        });

    }
    res.status(500).json({ error: "movie not found" })
});

module.exports = router