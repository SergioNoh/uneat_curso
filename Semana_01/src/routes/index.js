const { Router } = require('express')

const router = new Router();

router.get('/test', (req, res) => {
    const data = {
        name: "sergio",
        clase: "01"
    }
    res.json(data)
})

module.exports = router;