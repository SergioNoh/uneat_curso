const express = require('express');
const morgan = require('morgan');

const app = express();

app.set('port', process.env.PORT || 3000);

//middleware
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// routes
// app.get('/', (req, res) => {
//     res.json({ "title": "Hello World" })
// });
app.use(require('./routes/index.js'))
app.use('/api/movies', require('./routes/movies.js'))

console.log(module.exports)

// //starting the server
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
})