const express = require('express');
const morgan = require('morgan');

const app = express();

app.set('port', process.env.PORT || 3000)

//middleware
app.use(morgan('dev'))
app.use(express.json())


app.use('/pokemon/',require('./routes/pokeapi.js'))


app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
})