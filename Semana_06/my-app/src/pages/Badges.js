import React from 'react';
import {Link} from 'react-router-dom'

import './styles/Badges.css';
// import Navbar from '../components/Navbar';
import IdListClase from '../components/IdListClase';

class Badges extends React.Component {
  state = {
    data: [
      {
        id: '2de30c42-9deb-40fc-a41f-05e62b5939a7',
        firstName: 'Billy',
        lastName: 'Ordores',
        email: 'billy.ordores@alumnos.uneatlantico.es',
        jobTitle: 'CEO',
        twitter: 'BillyOrdores',
        avatarUrl:'https://www.gravatar.com/avatar/e74e87d40e55b9ff9791c78892e55cb7?d=identicon',
      },
      {
        id: 'd00d3614-101a-44ca-b6c2-0be075aeed3d',
        firstName: 'Alex',
        lastName: 'Bolanos',
        email: 'alex.bolanos@alumnos.uneatlantico.es',
        jobTitle: 'Computer Science Engineer',
        twitter: 'AlexBolanos',
        avatarUrl:'https://www.gravatar.com/avatar/e74e87d40e55b9ff9791c78892e55cb7?d=identicon',
      },
      {
        id: '83c03386-33a2-4512-9ac1-354ad7bec5e9',
        firstName: 'Alex',
        lastName: 'Diaz',
        email: 'alex.diaz1@alumnos.uneatlantico.es',
        jobTitle: 'Computer Science Engineer',
        twitter: 'AlexDiaz',
        avatarUrl:'https://www.gravatar.com/avatar/e74e87d40e55b9ff9791c78892e55cb7?d=identicon',
      },
      {
        id: '63c0436-33a2-4516-9gfc1-354yk7vmu5e9',
        firstName: 'Gustavo',
        lastName: 'Olaya',
        email: 'gustavo.olaya@alumnos.uneatlantico.es',
        jobTitle: 'National Markets Officer',
        twitter: 'GustavoOlaya',
        avatarUrl:'https://www.gravatar.com/avatar/e74e87d40e55b9ff9791c78892e55cb7?d=identicon',
      },
    ],
  };

  render() {
    return (
      <div>

        <div className="Badges__container">
          <div className="Badges__buttons">
            <Link to="/badges/new" className="btn btn-primary">
              New Badge
            </Link>
          </div>

          <IdListClase badges={this.state.data} />
        </div>
      </div>
    );
  }
}

export default Badges;
