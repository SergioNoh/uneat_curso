import React from 'react';

import IdCard from '../components/IdCard'
import IdForm from '../components/IdForm'

class IdCreator extends React.Component{
  state={form: {}}

  handleChange = e => {
    // console.log({
    //   [e.target.name]: e.target.value
    // })

    this.setState({
      form: {
        ...this.state.form,//investigar sintaxis
        [e.target.name]: e.target.value,
      }
    })
  }

    render(){
        return (
           <div>
            <div className="container">
            <div className="row">
            <div className="col-6">
              <IdCard
                firstname= {this.state.form.firstName}
                lastname= {this.state.form.lastName}
                email = "gustavo.olaya@gmail.com"
              />
            </div> 

            <div className="col-6">
              <IdForm onChange={this.handleChange}/>
            </div>
          </div>
        </div>
           </div>
        );
    }
}
export default IdCreator;