/* eslint-disable react/prop-types */
import React from 'react';

import './styles/IdCard.css'

class IdCard extends React.Component {
    render(){
        return (
        <div className="IdCard">
        <div className="IdCard__section-name">
          <img
            className="IdCard__avatar"
            src="http://0.gravatar.com/avatar/21163c44e23cc1c1315cb2f80aed15ee"
            alt="Avatar"
          />
          <h1>
            {this.props.firstname} <br /> {this.props.lastname}
          </h1>
        </div>
        <div className= "IdCard__section-info">
          <h3>Computer Science Engineer</h3>
          <div>@NasPyrric</div>
        </div>
        <div className="IdCard__footer ">#CursoUneat</div>
      </div>
        );
    }
}

export default IdCard

