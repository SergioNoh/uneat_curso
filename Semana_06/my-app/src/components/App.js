import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'


import IdCreator from '../pages/IdCreator'
import Badges from '../pages/Badges'

function App(){
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path= "/badges" component={Badges}/>
                <Route exact path= "/badges/new" component={IdCreator}/>
            </Switch>
        </BrowserRouter>
    )
}

export default App