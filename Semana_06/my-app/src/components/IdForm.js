import React from 'react';


// TAREAS:
// form -> twitter, puesto de trabajo, email.
// navbar
// pages -> home, about; con su navbar

class IdForm extends React.Component {
  state = {};
  handleChange = e => {
    // console.log({
    //   name: e.target.name,
    //   value: e.target.value,
    // });
    this.setState({
      [e.target.name]: e.target.value,
    })
  };

  handleClick = e => {
    e.preventDefault()
    console.log('Button was clicked');
    console.log(this.state)
  };

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>First Name</label>
            <input
              onChange={this.props.onChange}
              className="form-control"
              type="text"
              name="firstName"
              value={this.state.firstName}
            />
            <label>Last Name</label>
            <input
              onChange={this.props.onChange}
              className="form-control"
              type="text"
              name="lastName"
              value={this.state.lastName}
            />
          </div>

          <button onClick={this.handleClick} className="btn btn-primary">
            Save
          </button>
          </form>
      </div>
    );
  }
}

export default IdForm;
