import React from 'react';

import './styles/IdList.css';

class IdListItem extends React.Component{
  render(){
    return(
      <div>
        <h1>{this.props.badge.firstName} 
          {this.props.badge.lastName}
        </h1>
      </div>
    )
  }
}


class IdListClase extends React.Component {
  render() {
    return (
      <div claseName="IdList">
        <ul className="list-unstyled">
          {this.props.badges.map(badge => {
            return (
              <li key={badge.id}>
                <IdListItem badge={badge}/>
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

export default IdListClase;
