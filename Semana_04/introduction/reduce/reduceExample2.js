fs = require('fs')

let output = fs.readFileSync('data.txt','utf8')
.trim()
.split('\n')
.map(line => line.split(','))
.reduce((costumers,line)=>{
    costumers[line[0]] = costumers[line[0]] || []
    costumers[line[0]].push({
        name: line[1],
        price: line[2],
        quantity: line[3]
    })
    return costumers
},{})

console.log('output',output)