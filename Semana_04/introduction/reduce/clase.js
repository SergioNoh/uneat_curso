const fs = require('fs')

let output = fs.readFileSync('data.txt','utf8')
.trim()
.split('\n')
.map(line => line.split(','))
.reduce((clientes, line)=>{
    
    clientes[line[0]] = clientes[line[0]] || [];
    // console.log(clientes)
    clientes[line[0]].push(
        { 
            name: line[1],
            price: line[2],
            quantity: line[3]
        }
    )
    return clientes
},{})

//     console.log(clientes)

//     


//"sergio,andres,noh".split(',') --> ['sergio',"andres","noh"]

console.log('output:\n', output)

  

//   output {
//     'Henry Cavill': [
//       { name: 'coffe', price: '20', quantity: '5\r' },
//       { name: 'keyboard', price: '80', quantity: '1\r' },
//       { name: 'Monitor', price: '200', quantity: '2\r' }
//     ],
//     'Oliver Tree': [
//       { name: 'microphone', price: '250', quantity: '4\r' },
//       { name: 'tuner', price: '170', quantity: '2\r' },
//       { name: 'mouse', price: '90', quantity: '1' }
//     ]
//   }